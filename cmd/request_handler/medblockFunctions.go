package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/medblocks/medblockshyperledger/internal/database"
)

func addBlock(req database.Medblock, w http.ResponseWriter) {
	var err error

	permitted, err := database.PutMedblock(dBClient, req)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(req.Permissions) == len(permitted) {
		w.WriteHeader(http.StatusAccepted)
	} else {
		w.WriteHeader(http.StatusExpectationFailed)
		req.Permissions = permitted
	}
	returnVal, _ := json.Marshal(req)
	w.Write(returnVal)
}

func addPermission(req database.Medblock, w http.ResponseWriter) {
	var err error

	permitted, err := database.PutPermissions(dBClient, req.IPFSHash, req.Permissions)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(req.Permissions) == len(permitted) {
		w.WriteHeader(http.StatusAccepted)
	} else {
		w.WriteHeader(http.StatusExpectationFailed)
		req.Permissions = permitted
	}
	returnVal, _ := json.Marshal(req)
	w.Write(returnVal)
}

func listBlocks(ownerEmailID string, permittedEmailID string, w http.ResponseWriter) {
	byOwner, err := database.GetUserBlocks(dBClient, ownerEmailID)
	if err != nil {
		renderError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	byPermitted, err := database.GetPermittedBlocks(dBClient, permittedEmailID)
	if err != nil {
		renderError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var finalList []database.Medblock
	for _, vOwner := range byOwner {
		for _, vPermitted := range byPermitted {
			fmt.Println(vOwner.IPFSHash, vPermitted.IPFSHash)
			if vOwner.IPFSHash == vPermitted.IPFSHash {
				finalList = append(finalList, vOwner)
				break
			}
		}
	}
	response, _ := json.Marshal(finalList)
	w.Write(response)
	w.WriteHeader(http.StatusAccepted)
}
