package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/medblocks/medblockshyperledger/internal/database"
	"gitlab.com/medblocks/medblockshyperledger/internal/go-couchdb"
	"gitlab.com/medblocks/medblockshyperledger/internal/utils"
)

func userRequestInit(w http.ResponseWriter, r *http.Request) (req database.User, err error) {
	decoder := json.NewDecoder((*r).Body)
	err = decoder.Decode(&req)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: JSON user decode error: ", err), http.StatusBadRequest)
		return
	}
	dBClient, err = couchdb.NewClient("http://"+utils.DatabaseEndpoint+":5984", nil)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: DB client error: ", err), http.StatusInternalServerError)
	}
	return
}

func registerUserDb(req database.User) error {
	err := database.PutUser(dBClient, req)
	if err != nil {
		fmt.Println("REQUEST_HANDLER: add user error: ", err)
		return err
	}
	return nil
}

func registerUser(req database.User, w http.ResponseWriter) {
	var err error

	err = registerUserDb(req)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	returnVal, _ := json.Marshal(req)
	w.Write(returnVal)
	w.WriteHeader(http.StatusAccepted)
}

func storeKeysDb(req database.User) error {
	err := database.StoreKeys(dBClient, req)
	if err != nil {
		fmt.Println("REQUEST_HANDLER: add keys error: ", err)
		return err
	}
	return nil
}

func storeKeys(req database.User, w http.ResponseWriter) {
	var err error

	err = storeKeysDb(req)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	returnVal, _ := json.Marshal(req)
	w.Write(returnVal)
	w.WriteHeader(http.StatusAccepted)
}
