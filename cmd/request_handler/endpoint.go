package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/medblocks/medblockshyperledger/internal/go-couchdb"

	"gitlab.com/medblocks/medblockshyperledger/internal/database"
	"gitlab.com/medblocks/medblockshyperledger/internal/utils"
)

var dBClient *couchdb.Client

type signedRequests struct {
	Data      string `json:data`
	Signature string `json:signature`
}

func enableCors(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
}

func renderError(w http.ResponseWriter, message string, statusCode int) {
	w.WriteHeader(statusCode)
	fmt.Println(message)
	w.Write([]byte(message))
}

func signedRequestInit(w http.ResponseWriter, r *http.Request) (req signedRequests, err error) {
	decoder := json.NewDecoder((*r).Body)
	err = decoder.Decode(&req)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: JSON user decode error: ", err), http.StatusBadRequest)
		return
	}
	dBClient, err = couchdb.NewClient("http://"+utils.DatabaseEndpoint+":5984", nil)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: DB client error: ", err), http.StatusInternalServerError)
	}
	fmt.Println(req)
	fmt.Println(req.Data)
	return
}
func registerHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, err := userRequestInit(w, r)
	if err != nil {
		return
	}

	fmt.Println("Processing registration request...", req.SPublicKey)
	if req.SPublicKey == "" || req.EPublicKey == "" || req.EmailID == "" {
		missing := "Missing Required fields:"
		if req.EmailID == "" {
			missing += " emailId"
		}
		if req.SPublicKey == "" {
			missing += " sPublicKey"
		}
		if req.EPublicKey == "" {
			missing += " ePublicKey"
		}
		renderError(w, missing, http.StatusBadRequest)
		return
	}

	userExists, _ := database.GetUser(dBClient, req.EmailID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if userExists {
		renderError(w, fmt.Sprint("Already Exists: ", req.EmailID), http.StatusConflict)
		return
	}
	fmt.Println("Registration from request: ", req.EmailID)
	registerUser(req, w)
}

func getUserHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, err := userRequestInit(w, r)
	if err != nil {
		return
	}

	if req.EmailID == "" {
		missing := "Missing Required fields:"
		if req.EmailID == "" {
			missing += " emailId"
		}
		renderError(w, missing, http.StatusBadRequest)
		return
	}
	fmt.Println("Processing getUser request...", req.EmailID)

	userExists, userData := database.GetUser(dBClient, req.EmailID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !userExists {
		renderError(w, fmt.Sprint("Does not Exist: ", req.EmailID), http.StatusNotFound)
		return
	}
	fmt.Println("User from request: ", userData.EmailID)
	response, _ := json.Marshal(userData)
	w.Write(response)
	w.WriteHeader(http.StatusAccepted)
}

func addBlockHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, err := signedRequestInit(w, r)
	if err != nil {
		return
	}
	//TODO:Signature verification here
	var block database.Medblock
	json.Unmarshal([]byte(req.Data), &block)

	fmt.Println("Processing medblock insertion request...", block.IPFSHash)
	if block.IPFSHash == "" || block.Name == "" || block.Format == "" || block.CreatorEmailID == "" || block.OwnerEmailID == "" || block.Permissions == nil || block.IV == "" {
		missing := "Missing Required fields:"
		if block.IPFSHash == "" {
			missing += " ipfsHash"
		}
		if block.Name == "" {
			missing += " name"
		}
		if block.Format == "" {
			missing += " format"
		}
		if block.CreatorEmailID == "" {
			missing += " creatorEmailId"
		}
		if block.OwnerEmailID == "" {
			missing += " ownerEmailId"
		}
		if block.Permissions == nil {
			missing += " permissions"
		}
		if block.IV == "" {
			missing += " IV"
		}
		renderError(w, missing, http.StatusBadRequest)
		return
	}

	blockExists, _ := database.GetMedblock(dBClient, block.IPFSHash)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if blockExists {
		renderError(w, fmt.Sprint("Already Exists: ", block.IPFSHash), http.StatusConflict)
		return
	}
	fmt.Println("Block from request: ", block.IPFSHash)
	addBlock(block, w)
}
func getBlockHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	var req database.Medblock
	decoder := json.NewDecoder((*r).Body)
	err := decoder.Decode(&req)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: JSON block decode error: ", err), http.StatusBadRequest)
		return
	}
	dBClient, err = couchdb.NewClient("http://"+utils.DatabaseEndpoint+":5984", nil)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: DB client error: ", err), http.StatusInternalServerError)
	}
	if req.IPFSHash == "" {
		missing := "Missing Required fields:"
		if req.IPFSHash == "" {
			missing += " ipfsHash"
		}
		renderError(w, missing, http.StatusBadRequest)
		return
	}
	fmt.Println("Processing getBlock request...", req.IPFSHash)

	blockExists, block := database.GetMedblock(dBClient, req.IPFSHash)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !blockExists {
		renderError(w, fmt.Sprint("Does not Exist: ", req.IPFSHash), http.StatusNotFound)
		return
	}
	fmt.Println("Block from request: ", block.IPFSHash)
	response, _ := json.Marshal(block)
	w.Write(response)
	w.WriteHeader(http.StatusAccepted)
}
func getIdentityHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	var req database.User
	decoder := json.NewDecoder((*r).Body)
	err := decoder.Decode(&req)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: JSON block decode error: ", err), http.StatusBadRequest)
		return
	}
	dBClient, err = couchdb.NewClient("http://"+utils.DatabaseEndpoint+":5984", nil)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: DB client error: ", err), http.StatusInternalServerError)
	}
	if req.IdentityFileHash == "" {
		missing := "Missing Required fields:"
		if req.IdentityFileHash == "" {
			missing += " identityFileHash"
		}
		renderError(w, missing, http.StatusBadRequest)
		return
	}
	fmt.Println("Processing getIdentity request...", req.IdentityFileHash)

	blockExists, block := database.GetMedblock(dBClient, req.IdentityFileHash)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !blockExists {
		renderError(w, fmt.Sprint("Does not Exist: ", req.IdentityFileHash), http.StatusNotFound)
		return
	}
	fmt.Println("Identity from request: ", block.IPFSHash)
	response, _ := json.Marshal(block)
	w.Write(response)
	w.WriteHeader(http.StatusAccepted)
}
func updateIdentityHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, err := signedRequestInit(w, r)
	if err != nil {
		return
	}
	//TODO:Signature verification here
	var block database.User
	json.Unmarshal([]byte(req.Data), &block)

	fmt.Println("Processing identity update request...", block.EmailID)
	if block.IdentityFileHash == "" || block.EmailID == "" {
		missing := "Missing Required fields:"
		if block.IdentityFileHash == "" {
			missing += " ipfsHash"
		}
		if block.EmailID == "" {
			missing += " emailId"
		}
		renderError(w, missing, http.StatusBadRequest)
		return
	}

	blockExists, _ := database.GetMedblock(dBClient, block.IdentityFileHash)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !blockExists {
		renderError(w, fmt.Sprint("Identity block does not exist on the chain: ", block.IdentityFileHash), http.StatusConflict)
		return
	}
	userExists, userData := database.GetUser(dBClient, block.EmailID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !userExists {
		renderError(w, fmt.Sprint("Does not Exist: ", block.EmailID), http.StatusNotFound)
		return
	}
	userData.IdentityFileHash = block.IdentityFileHash
	err = database.PutUser(dBClient, userData)
	if err != nil {
		fmt.Println("REQUEST_HANDLER: update identity error: ", err)
		renderError(w, fmt.Sprint("update error: ", block.EmailID), http.StatusNotFound)
	}

	fmt.Println("Identity assigned from request: ", block.EmailID)
}
func addPermissionHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, err := signedRequestInit(w, r)
	if err != nil {
		return
	}
	//TODO:Signature verification here
	var block database.Medblock
	json.Unmarshal([]byte(req.Data), &block)

	fmt.Println("Processing medblock insertion request...", block.IPFSHash)
	if block.IPFSHash == "" || block.SenderEmailID == "" || block.Permissions == nil {
		missing := "Missing Required fields:"
		if block.IPFSHash == "" {
			missing += " ipfsHash"
		}
		if block.SenderEmailID == "" {
			missing += " senderEmailId"
		}
		if block.Permissions == nil {
			missing += " permissions"
		}
		renderError(w, missing, http.StatusBadRequest)
		return
	}

	blockExists, _ := database.GetMedblock(dBClient, block.IPFSHash)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !blockExists {
		renderError(w, fmt.Sprint("Block Does not Exist: ", block.IPFSHash), http.StatusNotFound)
		return
	}
	addPermission(block, w)
}
func listHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	var req database.Medblock
	decoder := json.NewDecoder((*r).Body)
	err := decoder.Decode(&req)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: JSON block decode error: ", err), http.StatusBadRequest)
		return
	}
	dBClient, err = couchdb.NewClient("http://"+utils.DatabaseEndpoint+":5984", nil)
	if err != nil {
		renderError(w, fmt.Sprintln("REQUEST_HANDLER: DB client error: ", err), http.StatusInternalServerError)
	}

	fmt.Println("Processing list request...", req.IPFSHash)
	listBlocks(req.OwnerEmailID, req.PermittedEmailID, w)
}

func storeKeyHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, err := userRequestInit(w, r)
	if err != nil {
		return
	}

	fmt.Println("Processing storeKey request...", req.EmailID)
	if req.EmailID == "" || req.SPrivateKey == "" || req.EPrivateKey == "" || req.IV == "" {
		missing := "Missing Required fields:"
		if req.EmailID == "" {
			missing += " emailId"
		}
		if req.SPrivateKey == "" {
			missing += " sPrivateKey"
		}
		if req.EPrivateKey == "" {
			missing += " ePrivateKey"
		}
		if req.IV == "" {
			missing += " IV"
		}
		renderError(w, missing, http.StatusBadRequest)
		return
	}

	userExists, _ := database.GetKeys(dBClient, req.EmailID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if userExists {
		renderError(w, fmt.Sprint("Already Exists: ", req.EmailID), http.StatusConflict)
		return
	}
	fmt.Println("Registration from request: ", req.EmailID)
	storeKeys(req, w)
}
func getKeyHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(w)
	defer r.Body.Close()
	req, err := userRequestInit(w, r)
	if err != nil {
		return
	}

	if req.EmailID == "" {
		missing := "Missing Required fields:"
		if req.EmailID == "" {
			missing += " emailId"
		}
		renderError(w, missing, http.StatusBadRequest)
		return
	}
	fmt.Println("Processing getKey request...", req.EmailID)

	userExists, userData := database.GetKeys(dBClient, req.EmailID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !userExists {
		renderError(w, fmt.Sprint("Does not Exist: ", req.EmailID), http.StatusNotFound)
		return
	}
	fmt.Println("Keys from request: ", userData.EmailID)
	response, _ := json.Marshal(userData)
	w.Write(response)
	w.WriteHeader(http.StatusAccepted)
}

func badGatewayHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusBadGateway)
	w.Write([]byte("Invalid gateway"))
}

func main() {
	docker := flag.Bool("docker", false, "pass to change endpoints to docker")
	flag.Parse()
	fmt.Println(*docker)
	utils.SetEndpoints(docker)
	http.HandleFunc("/register", registerHandler)
	http.HandleFunc("/getUser", getUserHandler)
	http.HandleFunc("/list", listHandler)
	http.HandleFunc("/storeKey", storeKeyHandler)
	http.HandleFunc("/getKey", getKeyHandler)
	http.HandleFunc("/addBlock", addBlockHandler)
	http.HandleFunc("/getBlock", getBlockHandler)
	http.HandleFunc("/getIdentity", getIdentityHandler)
	http.HandleFunc("/updateIdentity", updateIdentityHandler)
	http.HandleFunc("/addPermission", addPermissionHandler)
	http.HandleFunc("/", badGatewayHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
