package database

import (
	"fmt"
	"strconv"

	"gitlab.com/medblocks/medblockshyperledger/internal/go-couchdb"
)

type User struct {
	IV               string `json:"IV,omitempty"`
	EmailID          string `json:"emailId"`
	SPublicKey       string `json:"sPublicKey,omitempty"`
	EPublicKey       string `json:"ePublicKey,omitempty"`
	SPrivateKey      string `json:"sPrivateKey,omitempty"`
	EPrivateKey      string `json:"ePrivateKey,omitempty"`
	Rev              string `json:"_rev,omitempty"`
	IdentityFileHash string `json:"identityFileHash,omitempty"`
}

func PutUser(cl *couchdb.Client, doc User) error {
	db, err := cl.EnsureDB("user")
	if err != nil {
		return err
	}
	exists, usr := GetUser(cl, doc.EmailID)
	if exists {
		var temp string
		for _, v := range usr.Rev {
			if v != '-' {
				temp = temp + string(v)
			} else {
				break
			}
		}
		c, _ := strconv.Atoi(temp)
		_, err = db.Put(doc.EmailID, doc, strconv.Itoa(c+1))
	}
	_, err = db.Put(doc.EmailID, doc, "")
	return err
}

func GetUser(cl *couchdb.Client, EmailID string) (exists bool, user User) {
	db, err := cl.EnsureDB("user")
	if err != nil {
		return false, user
	}
	err = db.Get(EmailID, &user, nil)
	fmt.Println("YOYO", err)
	if err != nil {
		return false, user
	}
	return true, user
}

func StoreKeys(cl *couchdb.Client, doc User) error {
	db, err := cl.EnsureDB("key")
	if err != nil {
		return err
	}
	_, err = db.Put(doc.EmailID, doc, "")
	return err
}

func GetKeys(cl *couchdb.Client, EmailID string) (exists bool, user User) {
	db, err := cl.EnsureDB("key")
	if err != nil {
		return false, user
	}
	err = db.Get(EmailID, &user, nil)
	if err != nil {
		return false, user
	}
	return true, user
}
