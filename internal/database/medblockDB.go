package database

import (
	"fmt"

	"gitlab.com/medblocks/medblockshyperledger/internal/go-couchdb"
)

type Medblock struct {
	IPFSHash         string       `json:"ipfsHash,omitempty"`
	IV               string       `json:"IV,omitempty"`
	Name             string       `json:"name,omitempty"`
	Format           string       `json:"format,omitempty"`
	CreatorEmailID   string       `json:"creatorEmailId,omitempty"`
	OwnerEmailID     string       `json:"ownerEmailId,omitempty"`
	SenderEmailID    string       `json:"senderEmailId,omitempty"`
	PermittedEmailID string       `json:"permittedEmailId,omitempty"`
	Permissions      []Permission `json:"permissions,omitempty"`
}
type medblockResult struct {
	Medblocks []Medblock `json:"docs"`
}

func PutMedblock(cl *couchdb.Client, doc Medblock) (permitted []Permission, err error) {
	medblockDb, err := cl.EnsureDB("medblock")
	if err != nil {
		return
	}
	permissions := doc.Permissions
	doc.Permissions = nil
	_, err = medblockDb.Put(doc.IPFSHash, doc, "")
	if err != nil {
		return
	}
	permitted, err = PutPermissions(cl, doc.IPFSHash, permissions)
	return
}

func GetMedblock(cl *couchdb.Client, IPFSHash string) (exists bool, block Medblock) {
	medblockDb, err := cl.EnsureDB("medblock")
	if err != nil {
		fmt.Println(err)
		return false, block
	}
	permissionDb, err := cl.EnsureDB("permission")
	if err != nil {
		fmt.Println(err)
		return false, block
	}

	err = medblockDb.Get(IPFSHash, &block, nil)
	if err != nil {
		fmt.Println(err)
		return false, block
	}
	var permResponse permissionResult
	err = permissionDb.Find(fmt.Sprint(`
	{
		"selector": {
			"ipfsHash": "`, IPFSHash, `"
		},
		"fields": ["receiverEmailId", "receiverKey"]
	}
	`), &permResponse)

	if err != nil {
		fmt.Println(err)
		return false, block
	}
	block.Permissions = permResponse.Permissions
	return true, block
}

func GetUserBlocks(cl *couchdb.Client, ownerEmailId string) (list []Medblock, err error) {
	medblockDB, err := cl.EnsureDB("medblock")
	if err != nil {
		fmt.Println(err)
		return
	}
	var medblockResult medblockResult
	if ownerEmailId != "" {
		err = medblockDB.Find(fmt.Sprint(`
			{
				"selector":{
					"ownerEmailId": "`, ownerEmailId, `"
				},
				"fields": ["ipfsHash", "name", "format", "ownerEmailId"]
			}
		`), &medblockResult)
		fmt.Println(medblockResult)
		list = medblockResult.Medblocks
	} else {
		err = medblockDB.Find(fmt.Sprint(`
			{
				"selector":{},
				"fields": ["ipfsHash", "name", "format", "ownerEmailId"]
			}
		`), &medblockResult)
		fmt.Println(medblockResult)
		list = medblockResult.Medblocks
	}
	fmt.Println(list)
	return
}
