package database

import (
	"fmt"

	"gitlab.com/medblocks/medblockshyperledger/internal/go-couchdb"
)

type Permission struct {
	IPFSHash        string `json:"ipfsHash,omitempty"`
	ReceiverEmailID string `json:"receiverEmailId"`
	ReceiverKey     string `json:"receiverKey"`
}
type permissionResult struct {
	Permissions []Permission `json:"docs"`
}

func PutPermissions(cl *couchdb.Client, IPFSHash string, doc []Permission) (permitted []Permission, err error) {
	permissionDb, err := cl.EnsureDB("permission")
	if err != nil {
		return
	}
	for _, permission := range doc {
		permission.IPFSHash = IPFSHash
		_, err = permissionDb.Put(fmt.Sprint(IPFSHash, permission.ReceiverEmailID), permission, "")
		if err == nil {
			permitted = append(permitted, permission)
		}
	}
	return
}
func GetPermittedBlocks(cl *couchdb.Client, permittedEmailId string) (list []Permission, err error) {
	permissionDB, err := cl.EnsureDB("permission")
	if err != nil {
		fmt.Println(err)
		return
	}
	var permissionResult permissionResult
	if permittedEmailId != "" {
		err = permissionDB.Find(fmt.Sprint(`
			{
				"selector":{
					"receiverEmailId": "`, permittedEmailId, `"
				},
				"fields": ["ipfsHash"]
			}
		`), &permissionResult)
		fmt.Println(permissionResult)
		list = permissionResult.Permissions
	} else {
		err = permissionDB.Find(fmt.Sprint(`
			{
				"selector":{},
				"fields": ["ipfsHash"]
			}
		`), &permissionResult)
		fmt.Println(permissionResult)
		list = permissionResult.Permissions
	}
	fmt.Println(list)
	return
}
