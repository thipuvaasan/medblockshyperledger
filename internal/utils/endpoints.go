package utils

var DatabaseEndpoint = "localhost"

func SetEndpoints(docker *bool) {
	if *docker {
		DatabaseEndpoint = "couchdb"
	}
}
