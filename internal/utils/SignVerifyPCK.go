package utils

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
)

func verifySignature(message string, signature []byte, key *rsa.PublicKey) bool {
	hasher := sha256.New()
	hasher.Write([]byte(message))
	hashed := hasher.Sum(nil)
	err := rsa.VerifyPKCS1v15(
		key,
		crypto.SHA256,
		hashed,
		signature,
	)
	if err != nil {
		return false
	} else {
		return true
	}
}
