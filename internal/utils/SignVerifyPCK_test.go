package utils

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"os"
	"testing"
)

func getSignature(message string, key *rsa.PrivateKey) []byte {
	PCKS15Message := message
	hash := crypto.SHA256
	pssh := hash.New()
	pssh.Write([]byte(PCKS15Message))
	hashed := pssh.Sum(nil)
	signature, err := rsa.SignPKCS1v15(
		rand.Reader,
		key,
		hash,
		hashed,
	)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return signature
}
func TestTableVerifySignature(t *testing.T) {
	senderPrivateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		os.Exit(1)
	}
	fraudPrivateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		os.Exit(1)
	}
	senderPublicKey := &senderPrivateKey.PublicKey
	message := "Today is a good day to study encryption!"
	var tests = []struct {
		messageInput         string
		signatureInput       []byte
		senderPublicKeyInput *rsa.PublicKey
		expectedOutput       bool
	}{
		{message, getSignature(message, senderPrivateKey), senderPublicKey, true},
		{message, getSignature(message, fraudPrivateKey), senderPublicKey, false},
	}
	for _, test := range tests {
		if output := verifySignature(test.messageInput, test.signatureInput, test.senderPublicKeyInput); output != test.expectedOutput {
			t.Error("Test Failed!")
		}
	}
}
