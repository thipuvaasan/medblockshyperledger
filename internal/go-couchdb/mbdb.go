package couchdb

import(
	"flag"
	"encoding/json"
	"encoding/hex"
	"io/ioutil"
	"bytes"
	"crypto/md5"
	"fmt"
)

//user login
//func AddUser(doc string)
func DBLogin(username,password string) (*Client, error){
	user,pass := flag.String("u",username,"username"),flag.String("p",password,"password")
	client,err := NewClient("http://"+*user+":"+*pass+"@127.0.0.1:5984",nil)
	return client,err
}

type Spec struct{
	Emailid string `json:"emailid"`
	Privatekey string `json:"privatekey"`
	Publickey string `json:"publickey"`
}

type Entry struct{
	Id string `json:"_id"`
	Rev string `json:"_rev"`
	Emailid string `json:"emailid"`
	Privatekey string `json:"privatekey"`
	Publickey string `json:"publickey"`
}

type Val struct{
	Doc []Entry `json:"docs"`
	Bookmark string `json:"bookmark"`
	Warning string `json:"warning"`
}

type Sel struct {
	Row Row `json:"selector"`
}

type Row struct {
	Eid string `json:"emailid"`
}

//query privatekey of user using emailid
func (cl *Client) GetUser(emailid string) (bool,string,string,error){
	db,_ := cl.EnsureDB("user") 
	jsonstr := Sel{Row: Row{Eid: emailid,},}
	data,_ := json.Marshal(jsonstr)
	path := revpath("", db.name)
	path = path+"/_find"
	resp, err := db.request("POST", path, bytes.NewReader(data))
	if err != nil {
		return false,"","",err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false,"","",err
	}
	fmt.Println(string(body))
	var couchresp Val
	json.Unmarshal(body, &couchresp)
	fmt.Println(couchresp.Doc)
	//if couchresp
	if len(couchresp.Doc)==0{
			return false,"","",nil}
	return true,couchresp.Doc[0].Privatekey,couchresp.Doc[0].Publickey,nil
}
func (cl *Client) AddUser(doc interface{}) (string,error){
	db,_ := cl.EnsureDB("user") 
	return(db.addDoc(doc))
}
//add an entry to the user database, _id is md5 hash of emailid
func (db *DB) addDoc(doc interface{}) (string,error){
	//doc := `{"emailid":"admin@mb.com","privatekey":"111","publickey":"000"}`
	var jsonstr Spec
	byteValue, _ := json.Marshal(doc)
	json.Unmarshal(byteValue,&jsonstr)
	data := []byte(jsonstr.Emailid)
	sum := md5.Sum(data)
	nrev,err := db.Put(hex.EncodeToString(sum[:]),doc,"")
	return nrev,err
}