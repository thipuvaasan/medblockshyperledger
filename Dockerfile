FROM medblocks_dev_base
ARG build_cmd
ARG workdir=/ 
ADD . /go/src/gitlab.com/medblocks/medblockshyperledger/
WORKDIR $workdir
RUN $build_cmd
