# MedBlocks backend
MedBlocks backend packages meant run on the MedBlock nodes.

# Installation
* Clone branch "master"
* Run setup.sh, or alternatively run the command inside it in docker's console.
* Execute "Docker-compose build".

# Usage
From inside the medblocks_backend folder,
* Execute "Docker-compose up -d" to start the server. It should now be accessible from the front end.
* Execute "Docker-compose down" to stop the server.
