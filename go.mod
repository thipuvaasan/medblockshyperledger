module gitlab.com/medblocks/medblockshyperledger

go 1.12

require (
	github.com/cloudflare/cfssl v0.0.0-20190510060611-9c027c93ba9e // indirect
	github.com/fjl/go-couchdb v0.0.0-20140704151333-1f327c218d24
	github.com/fsouza/go-dockerclient v1.4.1 // indirect
	github.com/golang/mock v1.3.1 // indirect
	github.com/google/certificate-transparency-go v1.0.21 // indirect
	github.com/hyperledger/fabric v1.4.1 // indirect
	github.com/hyperledger/fabric-amcl v0.0.0-20181230093703-5ccba6eab8d6 // indirect
	github.com/hyperledger/fabric-lib-go v1.0.0 // indirect
	github.com/hyperledger/fabric-sdk-go v1.0.0-alpha5
	github.com/miekg/pkcs11 v1.0.2 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7 // indirect
	github.com/pkg/errors v0.8.1
	github.com/spf13/viper v1.4.0 // indirect
	github.com/sykesm/zap-logfmt v0.0.2 // indirect
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5 // indirect
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65 // indirect
	google.golang.org/genproto v0.0.0-20180831171423-11092d34479b // indirect
)
